"""
based on https://github.com/fhennecker/chars74k/blob/master/preprocessing.py
"""
import argparse
import os
import random
import re
import string
import shutil
from pathlib import Path

import cv2
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

CLASSES = string.digits + string.ascii_uppercase + string.ascii_lowercase


def load_filenames(datapath, filters=[]):
    """Loads all files contained in datapath where path contains all strings
    contained in filters"""

    filenames = []
    for path, dirs, files in os.walk(datapath):
        # filtering out unwanted paths
        if sum(map(lambda f: f in path, filters)) == len(filters):
            filenames += list(map(lambda f: path+'/'+f, files))
    return filenames


def split_and_save_dataset(dataset, filename):
    """Splits a dataset in 3 and saves lists of filenames."""
    splits = [0.6, 0.2, 0.2]
    split_names = ['train', 'validation', 'test']
    perm = np.random.permutation(len(dataset))
    
    for s, split in enumerate(splits):
        startindex = int(sum(splits[:s]) * len(dataset))
        endindex = int(startindex + splits[s] * len(dataset))
        with open(filename+'_'+split_names[s], 'w') as f:
            for i in perm[startindex:endindex]:
                f.write(dataset[i]+'\n')


def get_class_index(filename):
    return int(re.findall(r'.*img(\d+).*', filename)[0])-1


def get_class(filename):
    """Get the actual digit or character of the image"""
    return CLASSES[get_class_index(filename)]


def get_batch(dataset, batch_size, dimensions):
    batch_filenames = random.sample(dataset, batch_size)
    images = np.array(list(map(lambda f: open_image(f, dimensions), batch_filenames)))
    labels = np.array(list(map(get_class_index, batch_filenames)))
    return images, labels


def images_stats(dataset):
    """Print max width and height of all images in dataset"""
    max_width, max_height = 0, 0
    for img_name in dataset:
        img = cv2.imread(img_name)
        if img.shape[0] > max_height: max_height = img.shape[0]
        if img.shape[1] > max_width: max_width = img.shape[1]
    print("Max width : %d, max height: %d" % (max_width, max_height))


def open_image(filename, scale_to=[64, 64]):
    """Opens an image, returns the preprocessed image (scaled, masked)"""
    if "Fnt" in filename:
        img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, tuple(scale_to))
        processed_img = img.astype(np.float32)
        processed_img = np.expand_dims(processed_img, -1)

    else:
        img = cv2.imread(filename) * cv2.imread(filename.replace('Bmp', 'Msk')) / 255

        # scaling
        img = cv2.resize(img, tuple(scale_to))

        # normalising
        processed_img = img.astype(np.float32)
        for c in range(3):
            processed_img[:, :, c] /= np.max(processed_img[:, :, c])

        # to grayscale
        processed_img = cv2.cvtColor(
            (processed_img * 255).astype(np.uint8), cv2.COLOR_RGB2GRAY)
        processed_img = np.expand_dims(processed_img, -1)

    return processed_img


def mix(path="../../datasets/English", files="datasplits/mix"):
    """Create filenames for 'mix dataset', with real and digital font images."""
    filenames = load_filenames(path, ['Img', 'Good', 'Bmp'])
    print(len(filenames))
    filenames += load_filenames(path, ['Fnt'])
    print(len(filenames))
    split_and_save_dataset(filenames, files)


def fnt(path="../../datasets/English", files="datasplits/fnt"):
    """Create filenames for 'fnt dataset', with digital font images."""
    filenames = load_filenames(path, ['Fnt'])
    print(len(filenames))
    split_and_save_dataset(filenames, files)


def filenames_to_ds(ds_src_path="./chars74k-master", ds_dst_path="../../datasets/mychars",
                    filenames="./datasplits", prefix="good"):
    """Split dataset according to filenames."""
    os.chdir(ds_src_path)
    train_path = os.path.join(filenames, prefix + "_train")
    val_path = os.path.join(filenames, prefix + "_val")
    test_path = os.path.join(filenames, prefix + "_test")
    new_train = os.path.join(ds_dst_path, "train")
    new_val = os.path.join(ds_dst_path, "val")
    new_test = os.path.join(ds_dst_path, "test")
    old_paths = [train_path, val_path, test_path]
    new_paths = [new_train, new_val, new_test]

    for old, new in zip(old_paths, new_paths):
        with open(old) as f:
            files = f.read().splitlines()
            for img in files:
                name = os.path.basename(img)
                new_dir = img.split("/")[-2]
                new_path = os.path.join(new, new_dir)
                if not os.path.exists(new_path):
                    os.mkdir(new_path)
                new_name = os.path.join(new_path, name)
                shutil.copy2(img, new_name)


def clean_ds(ds_src_path, ds_dst_path, log_dir):
    """Remove images not needed for our task and rename directories. In the meantime, it print some stats."""
    df = pd.DataFrame()
    plt.figure()
    for subdir in ['train', 'test', 'val']:
        stats = dict()
        (Path(ds_src_path) / subdir).mkdir(exist_ok=True)
        class_dirs = sorted((Path(ds_src_path) / subdir).iterdir())
        for i, class_dir in enumerate(class_dirs):
            if i >= 36:
                break
            cls = CLASSES[i]
            dst_dir = Path(ds_dst_path) / subdir / cls
            if dst_dir.is_dir():
                shutil.rmtree(dst_dir)
            shutil.copytree(class_dir, dst_dir)
            num_imgs = len(list(dst_dir.iterdir()))
            print(f"Directory {subdir}-{cls}: {num_imgs} images")
            stats[cls] = num_imgs
        sub_df = pd.Series(stats)
        df[f"{subdir}_{int(sub_df.sum())}"] = sub_df
    print("\n", df, "\n", df.describe())
    df.to_csv(Path(log_dir) / (str(Path(ds_dst_path).name) + ".csv"))
    fig = df.plot.bar().get_figure()
    plt.show()
    fig.savefig(Path(log_dir) / (str(Path(ds_dst_path).name) + ".jpg"))


if __name__ == "__main__":
    # parser = argparse.ArgumentParser()
    # parser.add_argument('datapath')
    # parser.add_argument('-s', default="", help='Split and save dataset')
    # parser.add_argument('-t', action='store_true', help='Print dataset stats')
    #
    # opt = parser.parse_args()
    #
    # if opt.s:
    #     filenames = load_filenames(opt.datapath, ['Good', 'Bmp'])
    #     split_and_save_dataset(filenames, opt.s)
    # if opt.t:
    #     images_stats(filenames)

    # mix()
    # fnt()
    clean_ds("data/mychars", "data/my_chars74k", "data")
