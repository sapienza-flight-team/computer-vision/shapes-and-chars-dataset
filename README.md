# Shapes and Chars Dataset

We built a synthetic dataset based on https://github.com/ankush-me/SynthText to superimpose alphanumeric digital characters over geometric shapes made in cardboard, as needed for the AUVSI SUAS competition (https://www.auvsi-suas.org/).